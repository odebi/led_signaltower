# LED_SignalTower

## Name
LED SignalTower

## Description
LED Signal Tower anduino sketch and controller PC application.

## Usage
- Arduino GPIO port
  - 緑: 2
  - 黃: 3
  - 赤: 4
  - SW: 8 (3.3V負論理)
- Arduino USB serial:9600bps
  - serial command
  
| char | LED |
| ------ | ------ |
| 1 | 緑点灯       |
| 2 | 黃点灯       |
| 3 | 赤点灯       |
| 4 | 緑点滅(遅)       |
| 5 | 黃点滅(遅)       |
| 6 | 赤点滅(遅)       |
| 7 | 緑点滅(早)       |
| 8 | 黃点滅(早)       |
| 9 | 赤点滅(早)       |
| 0 | 消灯       |

- powershell script
  - signaltower制御スクリプト(signaltower.ps1)
    -  -comport [COMポート]
    -  -sendstring [送信文字列]

## Authors and acknowledgment
yas [at] odebi.net

## License
MIT License.

## Project status
Testing.
