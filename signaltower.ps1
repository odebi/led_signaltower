#
# signaltower制御スクリプト
#  -comport [COMポート]
#  -sendstring [送信文字列]

Param (
  [parameter(mandatory=$true)][string]$comport,
  [parameter(mandatory=$true)][string]$sendstring
)

# COMポートインスタンス作成
$c = New-Object System.IO.Ports.SerialPort $comport, 9600, ([System.IO.Ports.Parity]::None)

# フロー制御設定
$c.DtrEnable = $true
$c.RtsEnable = $true

# ハンドシェイク設定
$c.Handshake=[System.IO.Ports.Handshake]::None

# シリアルポートイベント設定
$d = Register-ObjectEvent -InputObject $c -EventName "DataReceived" `
    -Action {param([System.IO.Ports.SerialPort]$sender, [System.EventArgs]$e) `
        Write-Host -NoNewline $sender.ReadExisting()}

# COMポートを開く
$c.Open()

# 文字列送信
$c.Write($sendstring)

# COMポートを閉じる
$c.Close()

# イベント解除
Unregister-Event $d.Name

# ジョブ削除
Remove-Job $d.id

