#include <stdio.h>
#include <tchar.h>
#include <windows.h>

void serialSend(char *comStr, char *sendData) {
//	HANDLE comPort = CreateFile((wchar_t *)comStr, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	wchar_t comPort[32];
	const wchar_t* comStr1 = _T("\\\\.\\COM12");
	swprintf(comPort, 32, L"\\\\.\\%S", comStr);
	HANDLE com = CreateFile(comPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

	DCB dcb;								// シリアルポートの構成情報が入る構造体
	GetCommState(com, &dcb);				// 現在の設定値を読み込み

	// Serial設定項目(決め打ち)
	dcb.BaudRate = 9600;					// 速度
	dcb.ByteSize = 8;						// データ長
	dcb.Parity = NOPARITY;					// パリティ
	dcb.StopBits = ONESTOPBIT;				// ストップビット長
	dcb.fOutxCtsFlow = FALSE;				// 送信時CTSフロー
	dcb.fRtsControl = RTS_CONTROL_ENABLE;	// RTSフローを有効にしないとSPPが即切断される

	Sleep(1000);							// SPP接続とrfcomm起動待ち

	SetCommState(com, &dcb);				// 設定値反映

	DWORD sendLength = strlen(sendData);	// 送信するバイト数
	DWORD sentBytes;						// 送信したバイト数
	WriteFile(com, sendData, sendLength, &sentBytes, NULL); // ポートへ送信

	CloseHandle(com);						// ポートを閉じる
}

int main(int argc, char *argv[]) {
	if (argc == 3) {
		serialSend(argv[1], argv[2]);
		return 0;
	}

	return -1;
}
